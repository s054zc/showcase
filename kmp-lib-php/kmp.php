<?php

declare(strict_types=1);

class Kmp {
  /**
   * Host address to KMP portal.
   * 
   * @var KMP_HOST
   */
  const KMP_HOST  = 'http://portal.kmp-ab.se';

  /**
   * Data endpoint.
   * 
   * @var KMP_DATA
   */
  const KMP_DATA  = '/data.php';

  /**
   * Path to where commands are sent.
   * 
   * @var KMP_EXEC
   */
  const KMP_EXEC  = '/doit.php';

  /**
   * Cookie file.
   * 
   * @var COOKIE
   */
  const COOKIE    = 'COOKIE';

  /**
   * Delay value for poll-loop.
   * 
   * @var DELAY
   */
  const DELAY     = 5;

  /**
   * Constructor.
   * 
   * @param  void
   * @return no return value
   * @access public
   * @since  0.0.1
   * @author JH
   */
  public function __construct() { }

  /**
   * Destructor.
   * 
   * @param  void
   * @return no return value
   * @access public
   * @since  0.0.1
   * @author JH
   */
  public function __destruct() { }

  /**
   * A static method to get auth path.
   * 
   * @param  void
   * @return string
   * @access public
   * @since  0.0.1
   * @author JH
   * @static
   */
  public static function getAuthPath(): string {
    return self::KMP_HOST;
  }

  /**
   * A static method to get data path.
   * 
   * @param  void
   * @return string
   * @access public
   * @since  0.0.1
   * @author JH
   * @static
   */
  public static function getDataPath(): string {
    return self::KMP_HOST.self::KMP_DATA;
  }

  /**
   * A static method to get exec path.
   * 
   * @param  void
   * @return string
   * @access public
   * @since  0.0.1
   * @author JH
   * @static
   */
  public static function getExecPath(): string {
    return self::KMP_HOST.self::KMP_EXEC;
  }

  /**
   * A static method to set exec address.
   * 
   * @param  int
   * @return string
   * @access public
   * @since  0.0.1
   * @author JH
   * @static
   */
  public static function setExecAddr(int $arg0): string {
    return self::getExecPath()."?todo=$arg0";
  }

  /**
   * A static method to build sign in data string.
   * 
   * @param  array
   * @return string
   * @access public
   * @since  0.0.1
   * @author JH
   * @static
   */
  public static function buildSignInData(array $arg0): string {
    return sprintf(
      'kmpmac=%s&'    .
      'kmppwd=%s&'    .
      'stove=ANSLUT%%20KAMIN%%20%%2f%%20CONNECT%%20STOVE',
        $arg0['kmpmac'],
        $arg0['kmppwd']);
  }

  /**
   * Method to sign in to portal.
   * 
   * @param  void
   * @return void
   * @access public
   * @since  0.0.1
   * @author JH
   */
  public function signIn(array $arg0): void {
    $prm = self::buildSignInData($arg0);

    $ch = null;
    $ch = curl_init(self::getAuthPath());
    curl_setopt($ch, CURLOPT_VERBOSE,         0);
    curl_setopt($ch, CURLOPT_HEADER,          0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,  1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,  5);
    curl_setopt($ch, CURLOPT_TIMEOUT,         5);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,  1);
    curl_setopt($ch, CURLOPT_COOKIEJAR,       self::COOKIE);
    curl_setopt($ch, CURLOPT_POST,            1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,      $prm);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Host: portal.kmp-ab.se',
      'Connection: keep-alive',
      'Cache-Control: max-age=0',
      'Upgrade-Secure-Requests: 1',
      'Accept: text/html, application/json',
      'Accept-Language: en-US,en;q=0.9,sv;q=0.8,da=0.7']);
  
    $res = curl_exec($ch);

    /**
     * Check for socket failures.
     * On failure, an exception is thrown.
     * This check does not look for any HTTP
     * related error codes. That kind of error
     * check must be done after this check.
     */
    if (curl_errno($ch)) {
      $errno = curl_errno($ch);
      $error = curl_error($ch);
      curl_close($ch);

      throw new Exception(sprintf(
        'cURL failed! (%d): [%s]',
          $errno,
          $error));
    }

    /**
     * TODO:
     *  * Add result check ($res),
     *    not only connection check.
     *  * Check if user is signed in
     *    or not.
     */

    curl_close($ch);
  }

  /**
   * Method to sign out from portal.
   * 
   * @param  void
   * @return void
   * @access public
   * @since  0.0.1
   * @author JH
   */
  public function signOut(): void { }

  /**
   * Method to send commands to portal.
   * 
   * On failure, an exception is thrown.
   * 
   * @param  int
   * @return void
   * @access public
   * @since  0.0.1
   * @author JH
   */
  public function sendCmnd(int $arg0): void {
    $ch = null;
    $ch = curl_init(self::setExecAddr($arg0));
    curl_setopt($ch, CURLOPT_VERBOSE,         0);
    curl_setopt($ch, CURLOPT_HEADER,          0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,  1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,  5);
    curl_setopt($ch, CURLOPT_TIMEOUT,         5);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,  1);
    curl_setopt($ch, CURLOPT_COOKIEFILE,      self::COOKIE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Host: portal.kmp-ab.se',
      'Connection: keep-alive',
      'Cache-Control: max-age=0',
      'Upgrade-Secure-Requests: 1',
      'Accept: text/html, application/json',
      'Accept-Language: en-US,en;q=0.9,sv;q=0.8,da=0.7']);
    
    $res = curl_exec($ch);

    /**
     * Check for socket failures.
     * On failure, an exception is thrown.
     * This check does not look for any HTTP
     * related error codes. That kind of error
     * check must be done after this check.
     */
    if (curl_errno($ch)) {
      $errno = curl_errno($ch);
      $error = curl_error($ch);
      curl_close($ch);

      throw new Exception(sprintf(
        'cURL failed! (%d): [%s]',
          $errno,
          $error));
    }

    /**
     * TODO:
     *  * Add result check ($res),
     *    not only connection check.
     */

    curl_close($ch);
  }

  /**
   * Method to toggle stove on or off.
   * 
   * @param  void
   * @return void
   * @access public
   * @since  0.0.1
   * @author JH
   */
  public function toggleOnOff(): void {
    $this->sendCmnd(1);
  }

  /**
   * Method to step temperature up (+1).
   * 
   * @param  void
   * @return void
   * @access public
   * @since  0.0.1
   * @author JH
   */
  public function tempStepUp(): void {
    $this->sendCmnd(3);
  }

  /**
   * Method to step temperature down (-1).
   * @param  void
   * @return void
   * @access public
   * @since  0.0.1
   * @author JH
   */
  public function tempStepDn(): void {
    $this->sendCmnd(2);
  }

  /**
   * Method to send reset alarm.
   * 
   * @param  void
   * @return void
   * @access public
   * @since  0.0.1
   * @author JH
   */
  public function alarmReset(): void {
    $this->sendCmnd(27);
  }

  /**
   * Method to parse data from KMP portal.
   * 
   * @param  array
   * @return array
   * @access private
   * @since  0.0.1
   * @author JH
   */
  private function parse(array $arg0): array {
    $data = json_decode($arg0['json'], true);

    /**
     * Var/Key    Swe           Eng
     * =======    ===           ===
     *  lang      1             0
     *  mode      VILAR...      STANDBY
     *  glow      AVSTÄNGT      OFF
     *  
     *  alarm1    HÖG EFFEKT    HIGH HEAT
     *  alarm2    INGEN FLAMMA  NO ALARM
     */

    $ret = [
      'mode1'   => 0                        ,
      'mode2'   => ((int)   $data['mode2']) ,
      'glow'    => 0                        ,
      'feed'    => ((int)   $data['feed'])  ,
      'xfan'    => ((int)   $data['xFan'])  ,
      'cfan'    => ((int)   $data['cFan'])  ,
      'flue'    => ((int)   $data['tFlue']) ,
      'amps'    => ((float) $data['amps'])  ,
      'diff'    => ((int)   $data['tDiff']) ,
      'room'    => ((int)   $data['tRoom']) ,
      'stop'    => ((int)   $data['tStop']) ,
      'draft'   => ((int)   $data['draft']) ,
      'flame'   => ((int)   $data['tFlame']),
      'lang'    => ((int)   $data['lang'])  ,
      'time'    => strtotime($arg0['time'])];

    /**
     * Parse mode values.
     * 
     * Value      Description
     * =====      ===========
     *     1      Stand by
     *     2      Priming
     *     3      Igniting
     *     4      Warmup
     *     5      Alarm
     */
    $s0 = substr($arg0['mode1'] ?? ' ', 0, 2);
    $s1 = substr($arg0['mode1'] ?? ' ',   -2);
    if (preg_match('#^(ST|VI)$#', $s0))
      $ret['mode1'] = ((0 << 3) | 1);
    else if (preg_match('#^(LA|PR)$#', $s0))
      $ret['mode1'] = ((0 << 3) | 2);
    else if (preg_match('#^(TÄ|IG)$#', $s0))
      $ret['mode1'] = ((0 << 3) | 3);
    else if (preg_match('#^(AL|LA)$#', $s0))
      $ret['mode1'] = ((1 << 3) | 1);
    else if (preg_match('#^(OW|NG)$#', $s0))
      $ret['mode1'] = ((1 << 3) | 2);
    
    $a0 = $arg0['alarm1'];
    $a1 = $arg0['alarm2'];

    return $ret;
  }

  /**
   * Method to fetch data from KMP portal.
   * 
   * @param  callable
   * @return array
   * @access public
   * @since  0.0.1
   * @author JH
   */
  public function data(callable $arg0): array {
    $ch = null;
    $ch = curl_init(self::getDataPath());
    curl_setopt($ch, CURLOPT_VERBOSE,         0);
    curl_setopt($ch, CURLOPT_HEADER,          0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,  1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,  5);
    curl_setopt($ch, CURLOPT_TIMEOUT,         5);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,  1);
    curl_setopt($ch, CURLOPT_COOKIEFILE,      self::COOKIE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Host: portal.kmp-ab.se',
      'Connection: keep-alive',
      'Cache-Control: max-age=0',
      'Upgrade-Secure-Requests: 1',
      'Accept: text/html, application/json',
      'Accept-Language: en-US,en;q=0.9,sv;q=0.8,da=0.7']);
    
    /**
     * Enter the infinitive poll-loop.
     * A request is sent to the portal every N seconds,
     * specified in the 'DELAY' constant.
     * The data retrived from the portal is then passed
     * on to the parser method.
     */
    while (1) {
      $res = curl_exec($ch);

      var_dump($res);

      /**
       * Check for socket failures.
       * On failure, an exception is thrown.
       * This check does not look for any HTTP
       * related error codes. That kind of error
       * check must be done after this check.
       */
      if (curl_errno($ch)) {
        $errno = curl_errno($ch);
        $error = curl_error($ch);
        curl_close($ch);

        throw new Exception(sprintf(
          'cURL failed! (%d): [%s]',
            $errno,
            $error));
      }

      $data = json_decode(trim($res), true);

      $arg0($this->parse($data));

      sleep(self::DELAY);
    }

    return [];
  }
}