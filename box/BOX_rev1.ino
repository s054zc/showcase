
/**
 * B_1   L_1   B_2   L_2   B_3   L_3   B_4   L_4
 * (X) . (o) . (X) . (o) . (X) . (o) . (X) . (o)
 *                                               . (o)
 * (X) . (o) . (X) . (o) . (X) . (o) . (X) . (o)   L_9
 * B_5   L_5   B_6   L_6   B_7   L_7   B_8   L_8
 * 
 * (BTN_00) "B_1": 819 x0333
 * (BTN_01) "B_2": 853 x0355
 * (BTN_02) "B_3": 878 x036e
 * (BTN_03) "B_4": 896 x0380
 * (BTN_04) "B_5": 768 x0300
 * (BTN_05) "B_6": 683 x02ab
 * (BTN_06) "B_7": 513 x0201
 * (BTN_07) "B_8": 0   x0000
 * 
 * (LED_00) "L_1": 2   x02
 * (LED_01) "L_2": 3   x03
 * (LED_02) "L_3": 4   x04
 * (LED_03) "L_4": 5   x05
 * (LED_04) "L_5": 9   x09
 * (LED_05) "L_6": 8   x08
 * (LED_06) "L_7": 7   x07
 * (LED_07) "L_8": 6   x06
 * (LED_08) "L_9": 12  x0c
 * 
 * BITS 15-08:
 * b15..b09..: RESERVED.
 * b08 ......: LED_08_BIT
 * 
 * BITS 07-00:
 * b07 ......: LED_07_BIT
 * b06 ......: LED_06_BIT
 * b05 ......: LED_05_BIT
 * b04 ......: LED_04_BIT
 * b03 ......: LED_03_BIT
 * b02 ......: LED_02_BIT
 * b01 ......: LED_01_BIT
 * b00 ......: LED_00_BIT
 * 
 */

volatile unsigned short led_opt = 0;

#define LED_00_PIN  0x02
#define LED_01_PIN  0x03
#define LED_02_PIN  0x04
#define LED_03_PIN  0x05
#define LED_04_PIN  0x09
#define LED_05_PIN  0x08
#define LED_06_PIN  0x07
#define LED_07_PIN  0x06
#define LED_08_PIN  0x0c

#define LED_00_BIT  0x0001
#define LED_01_BIT  0x0002
#define LED_02_BIT  0x0004
#define LED_03_BIT  0x0008
#define LED_04_BIT  0x0010
#define LED_05_BIT  0x0020
#define LED_06_BIT  0x0040
#define LED_07_BIT  0x0080
#define LED_08_BIT  0x0100

#define RESET_ALL_LED_BIT led_opt  = 0;
#define TOGGLE_LED_00_BIT led_opt ^= LED_00_BIT
#define TOGGLE_LED_01_BIT led_opt ^= LED_01_BIT
#define TOGGLE_LED_02_BIT led_opt ^= LED_02_BIT
#define TOGGLE_LED_03_BIT led_opt ^= LED_03_BIT
#define TOGGLE_LED_04_BIT led_opt ^= LED_04_BIT
#define TOGGLE_LED_05_BIT led_opt ^= LED_05_BIT
#define TOGGLE_LED_06_BIT led_opt ^= LED_06_BIT
#define TOGGLE_LED_07_BIT led_opt ^= LED_07_BIT
#define TOGGLE_LED_08_BIT led_opt ^= LED_08_BIT

#define IS_LED_00_BIT_SET ((led_opt & LED_00_BIT) == LED_00_BIT)
#define IS_LED_01_BIT_SET ((led_opt & LED_01_BIT) == LED_01_BIT)
#define IS_LED_02_BIT_SET ((led_opt & LED_02_BIT) == LED_02_BIT)
#define IS_LED_03_BIT_SET ((led_opt & LED_03_BIT) == LED_03_BIT)
#define IS_LED_04_BIT_SET ((led_opt & LED_04_BIT) == LED_04_BIT)
#define IS_LED_05_BIT_SET ((led_opt & LED_05_BIT) == LED_05_BIT)
#define IS_LED_06_BIT_SET ((led_opt & LED_06_BIT) == LED_06_BIT)
#define IS_LED_07_BIT_SET ((led_opt & LED_07_BIT) == LED_07_BIT)
#define IS_LED_08_BIT_SET ((led_opt & LED_08_BIT) == LED_08_BIT)

#define BTNPIN      A0
#define BTN_XX      0x03ff
#define BTN_00      0x0333
#define BTN_01      0x0355
#define BTN_02      0x036e
#define BTN_03      0x0380
#define BTN_04      0x0300
#define BTN_05      0x02ab
#define BTN_06      0x0201
#define BTN_07      0x0000
#define BTNMRG      0x0001
#define BTN_XX_VAL  0xff
#define BTN_00_VAL  0x01
#define BTN_01_VAL  0x02
#define BTN_02_VAL  0x03
#define BTN_03_VAL  0x04
#define BTN_04_VAL  0x05
#define BTN_05_VAL  0x06
#define BTN_06_VAL  0x07
#define BTN_07_VAL  0x08

#define IS_BTN_XX(x)    ((x >= (BTN_XX-BTNMRG)) && (x <= (BTN_00+     0)))
#define IS_BTN_00(x)    ((x >= (BTN_00-BTNMRG)) && (x <= (BTN_00+BTNMRG)))
#define IS_BTN_01(x)    ((x >= (BTN_01-BTNMRG)) && (x <= (BTN_01+BTNMRG)))
#define IS_BTN_02(x)    ((x >= (BTN_02-BTNMRG)) && (x <= (BTN_02+BTNMRG)))
#define IS_BTN_03(x)    ((x >= (BTN_03-BTNMRG)) && (x <= (BTN_03+BTNMRG)))
#define IS_BTN_04(x)    ((x >= (BTN_04-BTNMRG)) && (x <= (BTN_04+BTNMRG)))
#define IS_BTN_05(x)    ((x >= (BTN_05-BTNMRG)) && (x <= (BTN_05+BTNMRG)))
#define IS_BTN_06(x)    ((x >= (BTN_06-BTNMRG)) && (x <= (BTN_06+BTNMRG)))
#define IS_BTN_07(x)    ((x >= (BTN_07-     0)) && (x <= (BTN_07+BTNMRG)))

#define BTN analogRead(BTNPIN)

/**
 * btn_0: Button pressed, value #0.
 * btn_1: Button pressed, value #1.
 * btn_c: Button pressed, current value.
 * btn_p: Button pressed, previous value.
 */
volatile unsigned char btn_0 = 0;
volatile unsigned char btn_1 = 0;
volatile unsigned char btn_c = 0;
volatile unsigned char btn_p = 0;

#define GAME1_STAT_0  0x00
#define GAME1_STAT_1  0x01
#define GAME1_STAT_2  0x02
#define GAME1_STAT_3  0x03
#define GAME1_STAT_7  0x07
#define GAME1_STAT_8  0x08

volatile unsigned char game1_stat     = 0;
volatile unsigned long game1_rand_min = 0L;
volatile unsigned long game1_rand_max = 127L;
volatile unsigned long game1_rand_val = 0L;
volatile unsigned char game1_path_end = 8;
volatile unsigned char game1_path_pos = 0;
volatile unsigned char game1_path_try = 0;

/**
 * Number of path's available; 128.
 */
volatile unsigned char path[][8] = {
  { BTN_07_VAL, BTN_02_VAL, BTN_00_VAL, BTN_03_VAL,
    BTN_06_VAL, BTN_01_VAL, BTN_04_VAL, BTN_05_VAL},
  { BTN_05_VAL, BTN_04_VAL, BTN_07_VAL, BTN_02_VAL,
    BTN_03_VAL, BTN_00_VAL, BTN_06_VAL, BTN_01_VAL},
  { BTN_03_VAL, BTN_06_VAL, BTN_00_VAL, BTN_07_VAL,
    BTN_02_VAL, BTN_04_VAL, BTN_01_VAL, BTN_05_VAL},
  { BTN_03_VAL, BTN_05_VAL, BTN_07_VAL, BTN_06_VAL,
    BTN_01_VAL, BTN_00_VAL, BTN_04_VAL, BTN_02_VAL},
  { BTN_01_VAL, BTN_04_VAL, BTN_06_VAL, BTN_07_VAL,
    BTN_03_VAL, BTN_00_VAL, BTN_02_VAL, BTN_05_VAL},
  { BTN_00_VAL, BTN_07_VAL, BTN_02_VAL, BTN_06_VAL,
    BTN_01_VAL, BTN_03_VAL, BTN_04_VAL, BTN_05_VAL},
  { BTN_03_VAL, BTN_05_VAL, BTN_01_VAL, BTN_02_VAL,
    BTN_07_VAL, BTN_00_VAL, BTN_04_VAL, BTN_06_VAL},
  { BTN_04_VAL, BTN_00_VAL, BTN_07_VAL, BTN_06_VAL,
    BTN_01_VAL, BTN_05_VAL, BTN_03_VAL, BTN_02_VAL},
  { BTN_01_VAL, BTN_05_VAL, BTN_02_VAL, BTN_04_VAL,
    BTN_06_VAL, BTN_00_VAL, BTN_03_VAL, BTN_07_VAL},
  { BTN_06_VAL, BTN_05_VAL, BTN_04_VAL, BTN_01_VAL,
    BTN_07_VAL, BTN_00_VAL, BTN_02_VAL, BTN_03_VAL},
  { BTN_02_VAL, BTN_01_VAL, BTN_07_VAL, BTN_06_VAL,
    BTN_05_VAL, BTN_04_VAL, BTN_03_VAL, BTN_00_VAL},
  { BTN_00_VAL, BTN_06_VAL, BTN_04_VAL, BTN_03_VAL,
    BTN_01_VAL, BTN_05_VAL, BTN_02_VAL, BTN_07_VAL},
  { BTN_05_VAL, BTN_00_VAL, BTN_02_VAL, BTN_04_VAL,
    BTN_06_VAL, BTN_07_VAL, BTN_01_VAL, BTN_03_VAL},
  { BTN_07_VAL, BTN_05_VAL, BTN_00_VAL, BTN_01_VAL,
    BTN_06_VAL, BTN_02_VAL, BTN_03_VAL, BTN_04_VAL},
  { BTN_06_VAL, BTN_07_VAL, BTN_05_VAL, BTN_03_VAL,
    BTN_00_VAL, BTN_04_VAL, BTN_01_VAL, BTN_02_VAL},
  { BTN_02_VAL, BTN_05_VAL, BTN_03_VAL, BTN_06_VAL,
    BTN_07_VAL, BTN_01_VAL, BTN_04_VAL, BTN_00_VAL},
  { BTN_01_VAL, BTN_02_VAL, BTN_07_VAL, BTN_05_VAL,
    BTN_03_VAL, BTN_06_VAL, BTN_00_VAL, BTN_04_VAL},
  { BTN_02_VAL, BTN_01_VAL, BTN_07_VAL, BTN_00_VAL,
    BTN_03_VAL, BTN_05_VAL, BTN_06_VAL, BTN_04_VAL},
  { BTN_07_VAL, BTN_06_VAL, BTN_03_VAL, BTN_04_VAL,
    BTN_02_VAL, BTN_05_VAL, BTN_00_VAL, BTN_01_VAL},
  { BTN_01_VAL, BTN_02_VAL, BTN_05_VAL, BTN_06_VAL,
    BTN_03_VAL, BTN_00_VAL, BTN_07_VAL, BTN_04_VAL},
  { BTN_02_VAL, BTN_06_VAL, BTN_03_VAL, BTN_00_VAL,
    BTN_07_VAL, BTN_01_VAL, BTN_05_VAL, BTN_04_VAL},
  { BTN_07_VAL, BTN_04_VAL, BTN_06_VAL, BTN_00_VAL,
    BTN_03_VAL, BTN_05_VAL, BTN_01_VAL, BTN_02_VAL},
  { BTN_02_VAL, BTN_04_VAL, BTN_03_VAL, BTN_05_VAL,
    BTN_06_VAL, BTN_00_VAL, BTN_01_VAL, BTN_07_VAL},
  { BTN_00_VAL, BTN_04_VAL, BTN_01_VAL, BTN_06_VAL,
    BTN_07_VAL, BTN_03_VAL, BTN_05_VAL, BTN_02_VAL},
  { BTN_02_VAL, BTN_05_VAL, BTN_03_VAL, BTN_04_VAL,
    BTN_07_VAL, BTN_01_VAL, BTN_06_VAL, BTN_00_VAL},
  { BTN_04_VAL, BTN_05_VAL, BTN_06_VAL, BTN_01_VAL,
    BTN_03_VAL, BTN_02_VAL, BTN_07_VAL, BTN_00_VAL},
  { BTN_02_VAL, BTN_06_VAL, BTN_01_VAL, BTN_05_VAL,
    BTN_00_VAL, BTN_03_VAL, BTN_04_VAL, BTN_07_VAL},
  { BTN_06_VAL, BTN_03_VAL, BTN_07_VAL, BTN_02_VAL,
    BTN_05_VAL, BTN_00_VAL, BTN_01_VAL, BTN_04_VAL},
  { BTN_05_VAL, BTN_02_VAL, BTN_04_VAL, BTN_03_VAL,
    BTN_01_VAL, BTN_06_VAL, BTN_07_VAL, BTN_00_VAL},
  { BTN_03_VAL, BTN_00_VAL, BTN_05_VAL, BTN_07_VAL,
    BTN_06_VAL, BTN_01_VAL, BTN_04_VAL, BTN_02_VAL},
  { BTN_02_VAL, BTN_03_VAL, BTN_06_VAL, BTN_00_VAL,
    BTN_01_VAL, BTN_04_VAL, BTN_07_VAL, BTN_05_VAL},
  { BTN_03_VAL, BTN_05_VAL, BTN_02_VAL, BTN_06_VAL,
    BTN_01_VAL, BTN_04_VAL, BTN_07_VAL, BTN_00_VAL},
  { BTN_05_VAL, BTN_04_VAL, BTN_07_VAL, BTN_03_VAL,
    BTN_02_VAL, BTN_06_VAL, BTN_00_VAL, BTN_01_VAL},
  { BTN_05_VAL, BTN_01_VAL, BTN_03_VAL, BTN_00_VAL,
    BTN_06_VAL, BTN_02_VAL, BTN_04_VAL, BTN_07_VAL},
  { BTN_02_VAL, BTN_00_VAL, BTN_07_VAL, BTN_05_VAL,
    BTN_03_VAL, BTN_01_VAL, BTN_06_VAL, BTN_04_VAL},
  { BTN_05_VAL, BTN_06_VAL, BTN_04_VAL, BTN_02_VAL,
    BTN_00_VAL, BTN_03_VAL, BTN_01_VAL, BTN_07_VAL},
  { BTN_07_VAL, BTN_05_VAL, BTN_04_VAL, BTN_01_VAL,
    BTN_06_VAL, BTN_00_VAL, BTN_03_VAL, BTN_02_VAL},
  { BTN_04_VAL, BTN_05_VAL, BTN_03_VAL, BTN_01_VAL,
    BTN_02_VAL, BTN_06_VAL, BTN_00_VAL, BTN_07_VAL},
  { BTN_00_VAL, BTN_01_VAL, BTN_05_VAL, BTN_04_VAL,
    BTN_02_VAL, BTN_06_VAL, BTN_07_VAL, BTN_03_VAL},
  { BTN_04_VAL, BTN_06_VAL, BTN_07_VAL, BTN_00_VAL,
    BTN_05_VAL, BTN_03_VAL, BTN_01_VAL, BTN_02_VAL},
  { BTN_05_VAL, BTN_02_VAL, BTN_03_VAL, BTN_00_VAL,
    BTN_04_VAL, BTN_06_VAL, BTN_01_VAL, BTN_07_VAL},
  { BTN_04_VAL, BTN_06_VAL, BTN_02_VAL, BTN_07_VAL,
    BTN_01_VAL, BTN_05_VAL, BTN_00_VAL, BTN_03_VAL},
  { BTN_00_VAL, BTN_03_VAL, BTN_05_VAL, BTN_04_VAL,
    BTN_07_VAL, BTN_06_VAL, BTN_01_VAL, BTN_02_VAL},
  { BTN_00_VAL, BTN_07_VAL, BTN_02_VAL, BTN_01_VAL,
    BTN_04_VAL, BTN_06_VAL, BTN_05_VAL, BTN_03_VAL},
  { BTN_04_VAL, BTN_00_VAL, BTN_05_VAL, BTN_07_VAL,
    BTN_03_VAL, BTN_02_VAL, BTN_01_VAL, BTN_06_VAL},
  { BTN_00_VAL, BTN_07_VAL, BTN_04_VAL, BTN_05_VAL,
    BTN_06_VAL, BTN_03_VAL, BTN_01_VAL, BTN_02_VAL},
  { BTN_07_VAL, BTN_06_VAL, BTN_04_VAL, BTN_03_VAL,
    BTN_02_VAL, BTN_00_VAL, BTN_01_VAL, BTN_05_VAL},
  { BTN_01_VAL, BTN_03_VAL, BTN_05_VAL, BTN_06_VAL,
    BTN_02_VAL, BTN_00_VAL, BTN_07_VAL, BTN_04_VAL},
  { BTN_05_VAL, BTN_07_VAL, BTN_06_VAL, BTN_03_VAL,
    BTN_00_VAL, BTN_04_VAL, BTN_02_VAL, BTN_01_VAL},
  { BTN_07_VAL, BTN_03_VAL, BTN_04_VAL, BTN_05_VAL,
    BTN_02_VAL, BTN_01_VAL, BTN_00_VAL, BTN_06_VAL},
  { BTN_01_VAL, BTN_03_VAL, BTN_02_VAL, BTN_07_VAL,
    BTN_04_VAL, BTN_00_VAL, BTN_06_VAL, BTN_05_VAL},
  { BTN_01_VAL, BTN_00_VAL, BTN_07_VAL, BTN_03_VAL,
    BTN_05_VAL, BTN_02_VAL, BTN_04_VAL, BTN_06_VAL},
  { BTN_00_VAL, BTN_05_VAL, BTN_07_VAL, BTN_02_VAL,
    BTN_06_VAL, BTN_04_VAL, BTN_01_VAL, BTN_03_VAL},
  { BTN_01_VAL, BTN_02_VAL, BTN_07_VAL, BTN_00_VAL,
    BTN_03_VAL, BTN_05_VAL, BTN_04_VAL, BTN_06_VAL},
  { BTN_07_VAL, BTN_00_VAL, BTN_01_VAL, BTN_04_VAL,
    BTN_03_VAL, BTN_02_VAL, BTN_05_VAL, BTN_06_VAL},
  { BTN_05_VAL, BTN_07_VAL, BTN_04_VAL, BTN_01_VAL,
    BTN_06_VAL, BTN_02_VAL, BTN_03_VAL, BTN_00_VAL},
  { BTN_02_VAL, BTN_07_VAL, BTN_01_VAL, BTN_05_VAL,
    BTN_00_VAL, BTN_04_VAL, BTN_03_VAL, BTN_06_VAL},
  { BTN_05_VAL, BTN_07_VAL, BTN_06_VAL, BTN_00_VAL,
    BTN_04_VAL, BTN_01_VAL, BTN_03_VAL, BTN_02_VAL},
  { BTN_03_VAL, BTN_07_VAL, BTN_00_VAL, BTN_05_VAL,
    BTN_04_VAL, BTN_01_VAL, BTN_06_VAL, BTN_02_VAL},
  { BTN_02_VAL, BTN_07_VAL, BTN_03_VAL, BTN_00_VAL,
    BTN_06_VAL, BTN_05_VAL, BTN_01_VAL, BTN_04_VAL},
  { BTN_00_VAL, BTN_07_VAL, BTN_02_VAL, BTN_06_VAL,
    BTN_04_VAL, BTN_05_VAL, BTN_01_VAL, BTN_03_VAL},
  { BTN_02_VAL, BTN_01_VAL, BTN_04_VAL, BTN_05_VAL,
    BTN_06_VAL, BTN_03_VAL, BTN_07_VAL, BTN_00_VAL},
  { BTN_05_VAL, BTN_07_VAL, BTN_02_VAL, BTN_04_VAL,
    BTN_06_VAL, BTN_01_VAL, BTN_00_VAL, BTN_03_VAL},
  { BTN_01_VAL, BTN_02_VAL, BTN_06_VAL, BTN_05_VAL,
    BTN_04_VAL, BTN_00_VAL, BTN_07_VAL, BTN_03_VAL},
  { BTN_00_VAL, BTN_04_VAL, BTN_05_VAL, BTN_01_VAL,
    BTN_06_VAL, BTN_03_VAL, BTN_02_VAL, BTN_07_VAL},
  { BTN_06_VAL, BTN_02_VAL, BTN_05_VAL, BTN_03_VAL,
    BTN_04_VAL, BTN_01_VAL, BTN_00_VAL, BTN_07_VAL},
  { BTN_01_VAL, BTN_06_VAL, BTN_00_VAL, BTN_05_VAL,
    BTN_04_VAL, BTN_02_VAL, BTN_03_VAL, BTN_07_VAL},
  { BTN_00_VAL, BTN_07_VAL, BTN_02_VAL, BTN_01_VAL,
    BTN_06_VAL, BTN_05_VAL, BTN_03_VAL, BTN_04_VAL},
  { BTN_06_VAL, BTN_05_VAL, BTN_03_VAL, BTN_02_VAL,
    BTN_04_VAL, BTN_00_VAL, BTN_01_VAL, BTN_07_VAL},
  { BTN_07_VAL, BTN_02_VAL, BTN_06_VAL, BTN_04_VAL,
    BTN_01_VAL, BTN_00_VAL, BTN_03_VAL, BTN_05_VAL},
  { BTN_03_VAL, BTN_01_VAL, BTN_00_VAL, BTN_06_VAL,
    BTN_05_VAL, BTN_07_VAL, BTN_04_VAL, BTN_02_VAL},
  { BTN_05_VAL, BTN_04_VAL, BTN_00_VAL, BTN_06_VAL,
    BTN_01_VAL, BTN_02_VAL, BTN_07_VAL, BTN_03_VAL},
  { BTN_05_VAL, BTN_03_VAL, BTN_06_VAL, BTN_00_VAL,
    BTN_02_VAL, BTN_04_VAL, BTN_01_VAL, BTN_07_VAL},
  { BTN_04_VAL, BTN_05_VAL, BTN_02_VAL, BTN_01_VAL,
    BTN_07_VAL, BTN_00_VAL, BTN_06_VAL, BTN_03_VAL},
  { BTN_02_VAL, BTN_01_VAL, BTN_06_VAL, BTN_00_VAL,
    BTN_07_VAL, BTN_05_VAL, BTN_04_VAL, BTN_03_VAL},
  { BTN_05_VAL, BTN_02_VAL, BTN_01_VAL, BTN_04_VAL,
    BTN_06_VAL, BTN_00_VAL, BTN_07_VAL, BTN_03_VAL},
  { BTN_00_VAL, BTN_04_VAL, BTN_02_VAL, BTN_03_VAL,
    BTN_05_VAL, BTN_07_VAL, BTN_06_VAL, BTN_01_VAL},
  { BTN_03_VAL, BTN_02_VAL, BTN_07_VAL, BTN_00_VAL,
    BTN_06_VAL, BTN_05_VAL, BTN_04_VAL, BTN_01_VAL},
  { BTN_00_VAL, BTN_04_VAL, BTN_03_VAL, BTN_07_VAL,
    BTN_01_VAL, BTN_06_VAL, BTN_05_VAL, BTN_02_VAL},
  { BTN_02_VAL, BTN_01_VAL, BTN_07_VAL, BTN_04_VAL,
    BTN_03_VAL, BTN_05_VAL, BTN_06_VAL, BTN_00_VAL},
  { BTN_02_VAL, BTN_01_VAL, BTN_05_VAL, BTN_06_VAL,
    BTN_00_VAL, BTN_07_VAL, BTN_03_VAL, BTN_04_VAL},
  { BTN_05_VAL, BTN_01_VAL, BTN_02_VAL, BTN_00_VAL,
    BTN_06_VAL, BTN_07_VAL, BTN_04_VAL, BTN_03_VAL},
  { BTN_00_VAL, BTN_06_VAL, BTN_01_VAL, BTN_07_VAL,
    BTN_02_VAL, BTN_05_VAL, BTN_03_VAL, BTN_04_VAL},
  { BTN_03_VAL, BTN_01_VAL, BTN_00_VAL, BTN_02_VAL,
    BTN_05_VAL, BTN_07_VAL, BTN_04_VAL, BTN_06_VAL},
  { BTN_07_VAL, BTN_00_VAL, BTN_03_VAL, BTN_06_VAL,
    BTN_02_VAL, BTN_05_VAL, BTN_01_VAL, BTN_04_VAL},
  { BTN_02_VAL, BTN_06_VAL, BTN_07_VAL, BTN_05_VAL,
    BTN_03_VAL, BTN_00_VAL, BTN_01_VAL, BTN_04_VAL},
  { BTN_05_VAL, BTN_03_VAL, BTN_01_VAL, BTN_04_VAL,
    BTN_02_VAL, BTN_06_VAL, BTN_00_VAL, BTN_07_VAL},
  { BTN_05_VAL, BTN_06_VAL, BTN_07_VAL, BTN_03_VAL,
    BTN_04_VAL, BTN_02_VAL, BTN_01_VAL, BTN_00_VAL},
  { BTN_04_VAL, BTN_05_VAL, BTN_00_VAL, BTN_01_VAL,
    BTN_03_VAL, BTN_07_VAL, BTN_06_VAL, BTN_02_VAL},
  { BTN_02_VAL, BTN_05_VAL, BTN_00_VAL, BTN_06_VAL,
    BTN_01_VAL, BTN_07_VAL, BTN_03_VAL, BTN_04_VAL},
  { BTN_03_VAL, BTN_00_VAL, BTN_01_VAL, BTN_02_VAL,
    BTN_07_VAL, BTN_04_VAL, BTN_05_VAL, BTN_06_VAL},
  { BTN_05_VAL, BTN_04_VAL, BTN_01_VAL, BTN_02_VAL,
    BTN_03_VAL, BTN_06_VAL, BTN_07_VAL, BTN_00_VAL},
  { BTN_03_VAL, BTN_00_VAL, BTN_01_VAL, BTN_02_VAL,
    BTN_05_VAL, BTN_04_VAL, BTN_07_VAL, BTN_06_VAL},
  { BTN_06_VAL, BTN_05_VAL, BTN_04_VAL, BTN_07_VAL,
    BTN_01_VAL, BTN_00_VAL, BTN_03_VAL, BTN_02_VAL},
  { BTN_04_VAL, BTN_01_VAL, BTN_03_VAL, BTN_07_VAL,
    BTN_00_VAL, BTN_02_VAL, BTN_06_VAL, BTN_05_VAL},
  { BTN_07_VAL, BTN_06_VAL, BTN_05_VAL, BTN_04_VAL,
    BTN_01_VAL, BTN_03_VAL, BTN_00_VAL, BTN_02_VAL},
  { BTN_03_VAL, BTN_01_VAL, BTN_07_VAL, BTN_00_VAL,
    BTN_05_VAL, BTN_04_VAL, BTN_06_VAL, BTN_02_VAL},
  { BTN_03_VAL, BTN_05_VAL, BTN_07_VAL, BTN_00_VAL,
    BTN_06_VAL, BTN_02_VAL, BTN_04_VAL, BTN_01_VAL},
  { BTN_01_VAL, BTN_05_VAL, BTN_06_VAL, BTN_07_VAL,
    BTN_03_VAL, BTN_00_VAL, BTN_04_VAL, BTN_02_VAL},
  { BTN_04_VAL, BTN_00_VAL, BTN_02_VAL, BTN_07_VAL,
    BTN_03_VAL, BTN_05_VAL, BTN_06_VAL, BTN_01_VAL},
  { BTN_02_VAL, BTN_04_VAL, BTN_03_VAL, BTN_01_VAL,
    BTN_07_VAL, BTN_06_VAL, BTN_05_VAL, BTN_00_VAL},
  { BTN_03_VAL, BTN_02_VAL, BTN_05_VAL, BTN_00_VAL,
    BTN_04_VAL, BTN_01_VAL, BTN_06_VAL, BTN_07_VAL},
  { BTN_00_VAL, BTN_01_VAL, BTN_03_VAL, BTN_07_VAL,
    BTN_04_VAL, BTN_06_VAL, BTN_05_VAL, BTN_02_VAL},
  { BTN_02_VAL, BTN_06_VAL, BTN_03_VAL, BTN_07_VAL,
    BTN_01_VAL, BTN_05_VAL, BTN_00_VAL, BTN_04_VAL},
  { BTN_04_VAL, BTN_03_VAL, BTN_07_VAL, BTN_00_VAL,
    BTN_02_VAL, BTN_06_VAL, BTN_05_VAL, BTN_01_VAL},
  { BTN_06_VAL, BTN_01_VAL, BTN_03_VAL, BTN_04_VAL,
    BTN_00_VAL, BTN_07_VAL, BTN_02_VAL, BTN_05_VAL},
  { BTN_02_VAL, BTN_07_VAL, BTN_01_VAL, BTN_00_VAL,
    BTN_04_VAL, BTN_03_VAL, BTN_05_VAL, BTN_06_VAL},
  { BTN_03_VAL, BTN_02_VAL, BTN_05_VAL, BTN_04_VAL,
    BTN_06_VAL, BTN_00_VAL, BTN_01_VAL, BTN_07_VAL},
  { BTN_05_VAL, BTN_00_VAL, BTN_03_VAL, BTN_01_VAL,
    BTN_07_VAL, BTN_02_VAL, BTN_04_VAL, BTN_06_VAL},
  { BTN_00_VAL, BTN_04_VAL, BTN_06_VAL, BTN_07_VAL,
    BTN_05_VAL, BTN_03_VAL, BTN_01_VAL, BTN_02_VAL},
  { BTN_04_VAL, BTN_01_VAL, BTN_02_VAL, BTN_00_VAL,
    BTN_07_VAL, BTN_03_VAL, BTN_05_VAL, BTN_06_VAL},
  { BTN_03_VAL, BTN_06_VAL, BTN_02_VAL, BTN_00_VAL,
    BTN_04_VAL, BTN_01_VAL, BTN_05_VAL, BTN_07_VAL},
  { BTN_05_VAL, BTN_03_VAL, BTN_06_VAL, BTN_04_VAL,
    BTN_07_VAL, BTN_00_VAL, BTN_02_VAL, BTN_01_VAL},
  { BTN_05_VAL, BTN_01_VAL, BTN_04_VAL, BTN_07_VAL,
    BTN_02_VAL, BTN_06_VAL, BTN_03_VAL, BTN_00_VAL},
  { BTN_01_VAL, BTN_07_VAL, BTN_06_VAL, BTN_00_VAL,
    BTN_05_VAL, BTN_02_VAL, BTN_04_VAL, BTN_03_VAL},
  { BTN_00_VAL, BTN_02_VAL, BTN_05_VAL, BTN_04_VAL,
    BTN_07_VAL, BTN_03_VAL, BTN_06_VAL, BTN_01_VAL},
  { BTN_04_VAL, BTN_05_VAL, BTN_03_VAL, BTN_07_VAL,
    BTN_00_VAL, BTN_01_VAL, BTN_06_VAL, BTN_02_VAL},
  { BTN_04_VAL, BTN_07_VAL, BTN_05_VAL, BTN_06_VAL,
    BTN_00_VAL, BTN_02_VAL, BTN_01_VAL, BTN_03_VAL},
  { BTN_02_VAL, BTN_03_VAL, BTN_00_VAL, BTN_07_VAL,
    BTN_01_VAL, BTN_04_VAL, BTN_05_VAL, BTN_06_VAL},
  { BTN_04_VAL, BTN_01_VAL, BTN_00_VAL, BTN_02_VAL,
    BTN_07_VAL, BTN_03_VAL, BTN_05_VAL, BTN_06_VAL},
  { BTN_02_VAL, BTN_05_VAL, BTN_00_VAL, BTN_03_VAL,
    BTN_01_VAL, BTN_04_VAL, BTN_06_VAL, BTN_07_VAL},
  { BTN_01_VAL, BTN_05_VAL, BTN_02_VAL, BTN_06_VAL,
    BTN_03_VAL, BTN_04_VAL, BTN_07_VAL, BTN_00_VAL},
  { BTN_06_VAL, BTN_05_VAL, BTN_07_VAL, BTN_00_VAL,
    BTN_01_VAL, BTN_03_VAL, BTN_02_VAL, BTN_04_VAL},
  { BTN_05_VAL, BTN_02_VAL, BTN_00_VAL, BTN_01_VAL,
    BTN_07_VAL, BTN_06_VAL, BTN_04_VAL, BTN_03_VAL},
  { BTN_06_VAL, BTN_02_VAL, BTN_00_VAL, BTN_03_VAL,
    BTN_04_VAL, BTN_07_VAL, BTN_05_VAL, BTN_01_VAL},
  { BTN_00_VAL, BTN_02_VAL, BTN_03_VAL, BTN_01_VAL,
    BTN_07_VAL, BTN_04_VAL, BTN_05_VAL, BTN_06_VAL},
  { BTN_06_VAL, BTN_04_VAL, BTN_01_VAL, BTN_05_VAL,
    BTN_07_VAL, BTN_00_VAL, BTN_03_VAL, BTN_02_VAL},
  { BTN_01_VAL, BTN_07_VAL, BTN_00_VAL, BTN_05_VAL,
    BTN_04_VAL, BTN_03_VAL, BTN_02_VAL, BTN_06_VAL}
};

void led_update(void);
void led_clear_and_update(void);
void led_play1(void);
void led_play2(void);
void led_play3(void);
void led_play4(void);
void led_play5(void);
char get_btn_val(void);
void get_btn(void);

void game1_init(void);
void game1_exec(void);
void game1_reset_val(void);
void game1_reset_all(void);

void
led_update(void)
{
  digitalWrite(LED_00_PIN, (IS_LED_00_BIT_SET ? HIGH : LOW));
  digitalWrite(LED_01_PIN, (IS_LED_01_BIT_SET ? HIGH : LOW));
  digitalWrite(LED_02_PIN, (IS_LED_02_BIT_SET ? HIGH : LOW));
  digitalWrite(LED_03_PIN, (IS_LED_03_BIT_SET ? HIGH : LOW));
  digitalWrite(LED_04_PIN, (IS_LED_04_BIT_SET ? HIGH : LOW));
  digitalWrite(LED_05_PIN, (IS_LED_05_BIT_SET ? HIGH : LOW));
  digitalWrite(LED_06_PIN, (IS_LED_06_BIT_SET ? HIGH : LOW));
  digitalWrite(LED_07_PIN, (IS_LED_07_BIT_SET ? HIGH : LOW));
  digitalWrite(LED_08_PIN, (IS_LED_08_BIT_SET ? HIGH : LOW));
}

void
led_clear_and_update(void)
{
  RESET_ALL_LED_BIT;
  led_update();
}

void
led_play1(void)
{
  unsigned char ii = 0;
  
  led_clear_and_update();
  for (ii = 0; ii <= 10; ii++) {
    TOGGLE_LED_00_BIT;
    TOGGLE_LED_01_BIT;
    TOGGLE_LED_02_BIT;
    TOGGLE_LED_03_BIT;
    TOGGLE_LED_04_BIT;
    TOGGLE_LED_05_BIT;
    TOGGLE_LED_06_BIT;
    TOGGLE_LED_07_BIT;
    TOGGLE_LED_08_BIT;
    led_update();
    delay(40);
  }
  delay(250);
  TOGGLE_LED_00_BIT;
  TOGGLE_LED_04_BIT;
  led_update();
  delay(150);
  TOGGLE_LED_01_BIT;
  TOGGLE_LED_05_BIT;
  led_update();
  delay(150);
  TOGGLE_LED_02_BIT;
  TOGGLE_LED_06_BIT;
  led_update();
  delay(150);
  TOGGLE_LED_03_BIT;
  TOGGLE_LED_07_BIT;
  led_update();
  delay(150);
  TOGGLE_LED_08_BIT;
  led_update();
  delay(150);
  led_clear_and_update();
}

void
led_play2(void)
{
  unsigned char ii = 0;
  
  led_clear_and_update();
  for (ii = 0; ii <= 10; ii++) {
    TOGGLE_LED_00_BIT;
    TOGGLE_LED_01_BIT;
    TOGGLE_LED_04_BIT;
    TOGGLE_LED_05_BIT;
    led_update();
    delay(40);
  }
  led_clear_and_update();
}

void
led_play3(void)
{
  unsigned char ii = 0;

  led_clear_and_update();
  for (ii = 0; ii <= 10; ii++) {
    TOGGLE_LED_02_BIT;
    TOGGLE_LED_03_BIT;
    TOGGLE_LED_06_BIT;
    TOGGLE_LED_07_BIT;
    led_update();
    delay(40);
  }
  led_clear_and_update();
}

void
led_play4(void)
{
  unsigned char ii = 0;

  for (ii = 0; ii <= 15; ii++) {
    led_clear_and_update();
    switch (ii) {
      case 0: case  8: TOGGLE_LED_00_BIT; break ;
      case 1: case  9: TOGGLE_LED_01_BIT; break ;
      case 2: case 10: TOGGLE_LED_02_BIT; break ;
      case 3: case 11: TOGGLE_LED_03_BIT; break ;
      case 4: case 12: TOGGLE_LED_04_BIT; break ;
      case 5: case 13: TOGGLE_LED_05_BIT; break ;
      case 6: case 14: TOGGLE_LED_06_BIT; break ;
      case 7: case 15: TOGGLE_LED_07_BIT; break ;
    }
    led_update();
    delay(40);
  }
  led_clear_and_update();
}

void
led_play5(void)
{
  unsigned char ii = 0;

  for (ii = 0; ii <= 15; ii++) {
    led_clear_and_update();
    switch (ii) {
      case 0: case  8: TOGGLE_LED_00_BIT; break ;
      case 1: case  9: TOGGLE_LED_01_BIT; break ;
      case 2: case 10: TOGGLE_LED_02_BIT; break ;
      case 3: case 11: TOGGLE_LED_03_BIT; break ;
      case 4: case 12: TOGGLE_LED_07_BIT; break ;
      case 5: case 13: TOGGLE_LED_06_BIT; break ;
      case 6: case 14: TOGGLE_LED_05_BIT; break ;
      case 7: case 15: TOGGLE_LED_04_BIT; break ;
    }
    led_update();
    delay(80);
  }
  led_clear_and_update();
}

char
get_btn_val(void)
{
  if      (IS_BTN_00(BTN)) return BTN_00_VAL;
  else if (IS_BTN_01(BTN)) return BTN_01_VAL;
  else if (IS_BTN_02(BTN)) return BTN_02_VAL;
  else if (IS_BTN_03(BTN)) return BTN_03_VAL;
  else if (IS_BTN_04(BTN)) return BTN_04_VAL;
  else if (IS_BTN_05(BTN)) return BTN_05_VAL;
  else if (IS_BTN_06(BTN)) return BTN_06_VAL;
  else if (IS_BTN_07(BTN)) return BTN_07_VAL;
  
  return BTN_XX_VAL;
}

void
get_btn(void)
{
  btn_0 = 0;
  btn_1 = 0;
  btn_c = BTN_XX_VAL;
 
  while (IS_BTN_XX(BTN));

  if ((btn_0 = get_btn_val()) != btn_p) {
    delay(50);
    if (btn_0 == (btn_1 = get_btn_val())) {
      btn_p = btn_0;
      btn_c = btn_0;
    }
  }
}

/**
 * GAME #1
 * =======
 */
void
game1_init(void)
{
  game1_reset_all();
}

void
game1_exec(void)
{
  game1_reset_all();
  
  while (1) {
    if (game1_path_pos == game1_path_end) {
      delay(300);
      led_clear_and_update();
      TOGGLE_LED_08_BIT;
      led_update();
      delay(1400);
      led_play1();
      game1_reset_all();
        continue ;
    }

    get_btn();
    if (btn_c == BTN_XX_VAL)
      continue ;

    if (btn_c == path[game1_rand_val][game1_path_pos++]) {
      switch (btn_c) {
        case BTN_00_VAL: TOGGLE_LED_00_BIT; break ;
        case BTN_01_VAL: TOGGLE_LED_01_BIT; break ;
        case BTN_02_VAL: TOGGLE_LED_02_BIT; break ;
        case BTN_03_VAL: TOGGLE_LED_03_BIT; break ;
        case BTN_04_VAL: TOGGLE_LED_04_BIT; break ;
        case BTN_05_VAL: TOGGLE_LED_05_BIT; break ;
        case BTN_06_VAL: TOGGLE_LED_06_BIT; break ;
        case BTN_07_VAL: TOGGLE_LED_07_BIT; break ;
      }
      led_update();
    }
    else {
        game1_reset_val();
    }
  }
}

void
game1_reset_val(void)
{
  led_clear_and_update();
  
  game1_path_end = 8;
  game1_path_pos = 0;
  if ((game1_stat == GAME1_STAT_2) ||
      (game1_stat == GAME1_STAT_3)) {
        if (game1_path_try++ >= 17) {
          led_play2();
          game1_path_try = 0;
          game1_reset_all();
        }
  }
}

void
game1_reset_all(void)
{
  game1_reset_val();
  
  randomSeed(analogRead(A2));
  game1_rand_val = random(
    game1_rand_min,
    game1_rand_max
  );

  if ((game1_stat == GAME1_STAT_0) ||
      (game1_stat == GAME1_STAT_2)) {
        switch (path[game1_rand_val][game1_path_pos++]) {
          case BTN_00_VAL: TOGGLE_LED_00_BIT; break ;
          case BTN_01_VAL: TOGGLE_LED_01_BIT; break ;
          case BTN_02_VAL: TOGGLE_LED_02_BIT; break ;
          case BTN_03_VAL: TOGGLE_LED_03_BIT; break ;
          case BTN_04_VAL: TOGGLE_LED_04_BIT; break ;
          case BTN_05_VAL: TOGGLE_LED_05_BIT; break ;
          case BTN_06_VAL: TOGGLE_LED_06_BIT; break ;
          case BTN_07_VAL: TOGGLE_LED_07_BIT; break ;
        }
        led_update();
  }
}

void
setup(void)
{
  pinMode(LED_00_PIN, OUTPUT);
  pinMode(LED_01_PIN, OUTPUT);
  pinMode(LED_02_PIN, OUTPUT);
  pinMode(LED_03_PIN, OUTPUT);
  pinMode(LED_04_PIN, OUTPUT);
  pinMode(LED_05_PIN, OUTPUT);
  pinMode(LED_06_PIN, OUTPUT);
  pinMode(LED_07_PIN, OUTPUT);
  pinMode(LED_08_PIN, OUTPUT);
  pinMode(A0,         INPUT );

  /**
   * Game modes:
   *  + BTN_00_VAL: First position in path is not lit up.
   *                No help is available.
   *                GAME1_STAT_1
   *  + BTN_01_VAL: First position in path IS lit up,
   *                but the number of tries is set to 16.
   *                When the maximum amount of tries is reached,
   *                four led's will flash, and a new path will
   *                be generated.
   *                Help is available.
   *                GAME1_STAT_2
   *  + BTN_02_VAL: First position in path is not lit up,
   *                and the number of tries is set to 16.
   *                When the maximum amount of tries is reched,
   *                four led's will flash, and a new path will
   *                be generated.
   *                No help available.
   *                GAME1_STAT_3
   *  + BTN_03_VAL: NO ACTION!
   *  + BTN_04_VAL: NO ACTION!
   *  + BTN_05_VAL: NO ACTION!
   *  + BTN_06_VAL: Easter egg! Flash led's randomly.
   *                When done, the game starts as if no button is pressed.
   *                GAME1_STAT_0
   *  + BTN_07_VAL: Easter egg! Flash led's randomly.
   *                When done, the game starts as is no button is pressed.
   *                GAME1_STAT_0
   */
  get_btn();
  switch (btn_c) {
    case BTN_00_VAL: game1_stat = GAME1_STAT_1; break ;
    case BTN_01_VAL: game1_stat = GAME1_STAT_2; break ;
    case BTN_02_VAL: game1_stat = GAME1_STAT_3; break ;
    case BTN_06_VAL: led_play4();               break ;
    case BTN_07_VAL: led_play5();               break ;
    default:         game1_stat = GAME1_STAT_0; break ;
  }

  led_play1();
  game1_init();
  game1_exec();
}

void
loop(void)
{
}

