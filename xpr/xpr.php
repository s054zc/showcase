<?php

declare(strict_types=1);

date_default_timezone_set('Europe/Stockholm');

/**
 * xpr_sign_in
 * 
 * Function to sign in to Xpider.
 * Generate a file 'COOKIE' that contains
 * the session data to access data from
 * Xpider. To sign out, remove the COOKIE
 * file.
 * 
 * On failure, the function will terminate
 * with an error message and code.
 * On success, a COOKIE file with content
 * will be present.
 * 
 * Param list:
 *  xpr_user
 *  xpr_pass
 * 
 * @param  array
 * @return void
 * @author JH
 * @since  0.0.1
 */
function xpr_sign_in(array $arg0): void {
	$url = 'https://xpider.sj.se/login.php';

	$pf = sprintf(
		'screenx=1400&screeny=900&'			.
		'windowx=1400&windowy=900&target=&'	.
		'user=%s&pass=%s'					,
			urlencode($arg0['xpr_user']),
			urlencode($arg0['xpr_pass']));

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_VERBOSE,			0);
	curl_setopt($ch, CURLOPT_HEADER,			0);
	curl_setopt($ch, CURLOPT_TIMEOUT,			5);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,	5);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION,	1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,	1);
	curl_setopt($ch, CURLOPT_COOKIEJAR,			'COOKIE');
	curl_setopt($ch, CURLOPT_POST,				1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,		$pf);

	$res = curl_exec($ch);

	/**
	 * On failure, the function will terminate
	 * here and the error message and code will
	 * be written to screen.
	 */
	if ($res === false) {
		$error = curl_error($ch);
		$errno = curl_errno($ch);
		curl_close($ch);

		die(sprintf(
			'cURL failed: "%s" (%d)'."\n",
				$error,
				$errno
		));
	}

	curl_close($ch);
}

/**
 * xpr_fetch_tour
 * 
 * Function to fetch list of tours.
 * 
 * Param list:
 *  xpr_date  Format: YYYY-MM-DD
 *  xpr_role
 *  xpr_city
 *  xpr_dest
 * 
 * @param  array
 * @return void
 * @author JH
 * @since  0.0.1
 * @see    xpr_sign_in
 */
function xpr_fetch_tour(array $arg0): void {
	$url  = 'https://xpider.sj.se/turf.php';
	$url .= '?datum='.$arg0['xpr_date'];
	$url .= '&owner='.$arg0['xpr_city'];
	$url .= '&persKat='.$arg0['xpr_role'];

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_VERBOSE,			0);
	curl_setopt($ch, CURLOPT_HEADER,			0);
	curl_setopt($ch, CURLOPT_TIMEOUT,			5);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,	5);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION,	1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,	1);
	curl_setopt($ch, CURLOPT_COOKIEFILE,		'COOKIE');

	$res = curl_exec($ch);

	/**
	 * On failure, the function will terminate
	 * here and the error message and code will
	 * be written to screen.
	 */
	if ($res === false) {
		$error = curl_error($ch);
		$errno = curl_errno($ch);
		curl_close($ch);

		die(sprintf(
			'cURL failed: "%s" (%d)'."\n",
				$error,
				$errno
		));
	}

	curl_close($ch);

	$path  = $arg0['xpr_dest'].'/';
	$path .= $arg0['xpr_date'].'/'.date('H');
	if (!file_exists($path)) {
		if (!mkdir($path, 0755, true))
			die('Failed to create path.');
	}

	/**
	 * Parse response data, extract SRC
	 * attributes from IMG tags.
	 */
	$DOM = new DOMDocument();
	libxml_use_internal_errors(true);
	$DOM->loadHTML($res);
	libxml_use_internal_errors(false);
	$node = $DOM->getElementsByTagName('img');
	for ($ii = 0; $ii < $node->length; $ii++) {
		$src = trim($node[$ii]->attributes->item(0)->value);
		xpr_fetch_tour_gfx([
			'xpr_src'  => $src,
			'xpr_date' => $arg0['xpr_date'],
			'xpr_dest' => $path]);
	}


}

/**
 * xpr_fetch_tour_gfx
 * 
 * Function to fetch graphical tour data.
 * 
 * Param list:
 *  xpr_src
 *  xpr_dest
 * 
 * @param  array
 * @return void
 * @author JH
 * @since  0.0.1
 * @see    xpr_fetch_tour
 */
function xpr_fetch_tour_gfx(array $arg0): void {
	$url = 'https://xpider.sj.se/'.$arg0['xpr_src'];

	parse_str(preg_split('#\?#', $arg0['xpr_src'])[1], $prm);

	/**
	$file = sprintf(
		'%s/%s_%s_%06d_%s.png',
			$arg0['xpr_dest'],
			str_replace(
				0x2d, 0x4d, $prm['datum']),
			$prm['persKat'],
			$prm['persID'],
			$prm['turNr']);
	**/

	$file = sprintf(
		'%s/%s_%s_%06d_%s.png',
			$arg0['xpr_dest'],
			$prm['datum'],
			$prm['persKat'],
			$prm['persID'],
			$prm['turNr']);

echo $file."\n";

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_VERBOSE,			0);
	curl_setopt($ch, CURLOPT_HEADER,			0);
	curl_setopt($ch, CURLOPT_TIMEOUT,			5);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,	5);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION,	1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,	1);
	curl_setopt($ch, CURLOPT_COOKIEFILE,		'COOKIE');

	$res = curl_exec($ch);

	/**
	 * On failure, the function will terminate
	 * here and the error message and code will
	 * be written to screen.
	 */
	if ($res === false) {
		$error = curl_error($ch);
		$errno = curl_errno($ch);
		curl_close($ch);

		die(sprintf(
			'cURL failed: "%s" (%d)'."\n",
				$error,
				$errno
		));
	}

	curl_close($ch);

	$fd = fopen($file, 'wb');
	fwrite($fd, $res);
	fflush($fd);
	fclose($fd);
}

function __main__(array $arg0): void {
	echo '__main__'."\n";

	xpr_sign_in([
		'xpr_user' => 's054zc',
		'xpr_pass' => '']);

	$path = '.';
	if (isset($arg0[4]))
		$path = $arg0[4];
	xpr_fetch_tour([
		'xpr_date' => $arg0[1],
		'xpr_role' => $arg0[2],
		'xpr_city' => $arg0[3],
		'xpr_dest' => $path]);
}

__main__($argv);
