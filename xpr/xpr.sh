#!/bin/bash

if [ ! -z "$1" ]; then
	date="$1";
	echo $date;
	php -f xpr.php "$date" "lokf" "SUC" "./data/suc/lokf";
	php -f xpr.php "$date" "tm" "SUC" "./data/suc/tm";
	php -f xpr.php "$date" "lokf" "UÅ" "./data/uå/lokf";
	php -f xpr.php "$date" "tm" "UÅ" "./data/uå/tm";
	php -f xpr.php "$date" "lokf" "ÖS" "./data/ös/lokf";
	php -f xpr.php "$date" "tm" "ÖS" "./data/ös/tm";
	exit 0;
fi

step="3";
time="`date '+%s'`";
time=$[$time - 86400];
end=$[$time + (86400 * $step)];
while [ "$time" -lt "$end" ]; do
	date="`date -r $time '+%Y-%m-%d'`";
	time=$[$time + 86400];
	echo $date;
	php -f xpr.php "$date" "lokf" "SUC" "./data/suc/lokf";
	php -f xpr.php "$date" "tm" "SUC" "./data/suc/tm";
	php -f xpr.php "$date" "lokf" "UÅ" "./data/uå/lokf";
	php -f xpr.php "$date" "tm" "UÅ" "./data/uå/tm";
	php -f xpr.php "$date" "lokf" "ÖS" "./data/ös/lokf";
	php -f xpr.php "$date" "tm" "ÖS" "./data/ös/tm";
done

