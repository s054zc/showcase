# Showcase

## kmp-lib-php

Ett API för att kommunicera med pelletskaminen hemma.

Tillverkaren av pelletskaminen (KMP) har ett webbgränssnitt där användare kan styra sin pelletskamin från en annan plats än hemmet. Tanken med API:et var att utveckla ett eget gränssnitt och utveckla en app som nyttjar gränssnittet. Samt spara data för att sammanställa hur mycket och när kaminen används och hur mycket ström den drar med mera.

Lösningen är skriven i PHP.

## micro-os-512-bytes

Ett 16 bitars "operativsystem" på 512 bytes, som får plats på hårddiskens första sektor MBR - Master Boot Record.
Syftet med programvaran var att lära mig assembler, och hur operativsystem fungerar. Programvaran ritar upp ett enklare gränssnitt och placerar ut ett "X", som användaren sen kan styra med hjälp av tangentbordet. Skärmdumpar finns.

Programvaran är skriven i assembler, och har testats både på fysisk hårdvara och i virtuell miljö.

## xpr

En skriptlösning för att ladda ner turförtekningar för olika åkstationer på SJ AB.
Lösningen består av två stycken skript, ett skrivet i PHP och ett i bash. PHP-skriptet är huvudskriptet som hämtar ned önskad data. Shellskriptet används för att kunna utföra flera nedladdningar genom ett kommando.

Lösningen är skriven i PHP och bash.
