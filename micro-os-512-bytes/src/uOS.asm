[BITS 16]

[ORG 0x00]
	jmp 0x07C0:stage1

%define SCRN_W	0x50
%define SCRN_H	0x19

stage1:
	xor ax, ax
	mov ax, cs
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	mov ah, 0x00
	mov al, 0x03
	int 0x10

draw_ui:
	xor ax, ax
	mov ah, 0x02
	mov dh, 0x00
	mov dl, 0x00
	int 0x10

	xor ax, ax
	mov ah, 0x09
	mov al, 0x00
	mov bl, 0x1F
	mov cx, 0x50
	int 0x10

	.draw_header:
		xor ax, ax
		mov ax, SCRN_W
		mov cx, 0x0002
		div cx
		mov [.1], al

		xor ax, ax
		mov ax, word [s0_sz]
		mov cx, 0x0002
		div cx
		sub [.1], al

		mov ah, 0x02
		mov dh, 0x00
		mov dl, [.1]
		int 0x10
		mov si, s0
		call print

	xor ax, ax
	mov ah, 0x02
	mov dh, (SCRN_H-1)
	mov dl, 0x00
	int 0x10

	xor ax, ax
	mov ah, 0x09
	mov al, 0x00
	mov bh, 0x00
	mov bl, 0x1F
	mov cx, 0x50
	int 0x10

	.draw_footer:
		xor ax, ax
		mov ah, 0x02
		mov dh, (SCRN_H-1)
		mov dl, 0x00
		int 0x10
		mov si, s1
		call print

	xor ax, ax
	mov ah, 0x02
	mov dh, 0x01
	mov dl, 0x00
	int 0x10

	jmp short move_mark

	.1 db 0x00

move_mark:
	.mark db 'X'

	mov ah, 0x09
	mov al, [.mark]
	mov bx, 0x07
	mov cx, 0x01
	int 0x10

	.loop:
		call getch
		cmp al, 'w'
		je near .move_up
		cmp al, 's'
		je near .move_dn
		cmp al, 'a'
		je near .move_lt
		cmp al, 'd'
		je near .move_rt
		jmp near .loop

	.move_up:
		mov ah, 0x03
		int 0x10

		cmp dh, [scrn_sz_y0]
		je short .loop

		pusha
		mov ah, 0x09
		mov al, ' '
		mov bx, 0x07
		mov cx, 0x01
		int 0x10
		popa

		dec dh

		mov ah, 0x02
		mov dh, dh
		mov dl, dl
		int 0x10

		mov ah, 0x09
		mov al, [.mark]
		mov bx, 0x07
		mov cx, 0x01
		int 0x10
		jmp near .loop

	.move_dn:
		mov ah, 0x03
		int 0x10

		cmp dh, [scrn_sz_y1]
		je short .loop

		pusha
		mov ah, 0x09
		mov al, ' '
		mov bx, 0x07
		mov cx, 0x01
		int 0x10
		popa

		inc dh

		mov ah, 0x02
		mov dh, dh
		mov dl, dl
		int 0x10

		mov ah, 0x09
		mov al, [.mark]
		mov bx, 0x07
		mov cx, 0x01
		int 0x10
		jmp near .loop

	.move_lt:
		mov ah, 0x03
		int 0x10

		cmp dl, [scrn_sz_x0]
		je near .loop

		pusha
		mov ah, 0x09
		mov al, ' '
		mov bx, 0x07
		mov cx, 0x01
		int 0x10
		popa

		dec dl

		mov ah, 0x02
		mov dh, dh
		mov dl, dl
		int 0x10

		mov ah, 0x09
		mov al, [.mark]
		mov bx, 0x07
		mov cx, 0x01
		int 0x10
		jmp near .loop

	.move_rt:
		mov ah, 0x03
		int 0x10

		cmp dl, [scrn_sz_x1]
		je near .loop

		pusha
		mov ah, 0x09
		mov al, ' '
		mov bx, 0x07
		mov cx, 0x01
		int 0x10
		popa

		inc dl

		mov ah, 0x02
		mov dh, dh
		mov dl, dl
		int 0x10

		mov ah, 0x09
		mov al, [.mark]
		mov bx, 0x07
		mov cx, 0x01
		int 0x10
		jmp near .loop

	.done:
		ret

	jmp hang

getch:
	pusha
	mov ax, 0x00
	mov ah, 0x10
	int 0x16
	mov [.buf], ax
	popa
	mov ax, [.buf]
	ret

	.buf dw 0x00

; Use this way:
;  mov si, X
;  call print
print:
	pusha
	mov ah, 0x0E
	.loop:
		lodsb
		cmp al, 0x00
		je short .done
		int 0x10
		jmp short .loop
	.done:
		popa
		ret

hang:
	jmp hang

s0    db "uOS, version 0.1 - A 512 byte OS", 0x00
s0_sz db $-s0
s1    db "<A> left <D> right <W> up <S> down", 0x00
s1_sz db $-s1

;
; SCREEN:
;     Y0
;  X0    X1
;     Y1
;
scrn_sz_x0 db 0x00
scrn_sz_x1 db (SCRN_W-0x01)
scrn_sz_y0 db 0x01
scrn_sz_y1 db (SCRN_H-0x02)

times 510-($-$$) db 0x00
	db 0x55
	db 0xAA
