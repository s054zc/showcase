[bits 16]

[section .text]

[org 0]

	jmp 0x07c0:begin

;
; Section TEXT (Code)
;
[section .text]
begin:	xor ax, ax
				mov ax, cs
				mov ds, ax
				mov es, ax
				mov fs, ax
				mov gs, ax

				xor	ax, ax
				mov ah, 0x00
				mov	al, 0x03
				int	0x10

				mov	si, S0
				call	echo

				jmp	halt

echo:		cld
	.L0:	lodsb
				cmp	al, 0x00
				je	.L1
				mov	ah, 0x0e
				int	0x10
				jmp	.L0
	.L1:	ret

halt:		hlt
				jmp halt

;
; Section DATA
;
[section .data]
V0:		db	0x33
V1:		db	0x44
S0:		db	"A S T O S", 0
;
; Section BSS
;
;[section .bss]
t0:	resb	2
t1:	resb	4

;
; Padd and sign.
;
times 510-($-$$) db 0
	db 0x55
	db 0xaa


